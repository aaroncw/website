Aaron's Website
===============

This is the source code for my website! You can see the live version here: http://aaronwillows.com/ or go to my bitbucket account at https://bitbucket.org/aaroncw/


Building
--------
Building this software requires [NodeJS], [GruntJS], [Bower] and [Yeoman]. It is based on a forked project called [ngBoilerplate]. 

Once node is installed, you can follow these steps to build the application:

** Install Dependencies**
```sh
npm install -g grunt-cli
npm install -g bower
npm install
```

** Live Preview** 
```sh
grunt watch
```
This will start a webserver at http://localhost:4001/ 

** Development Build** 
```sh
grunt build
```

** Optimized Build **
```sh
grunt compile
```


License
--------
Feel free to download the source code and give it to others. It's all open source and licensed under the MIT license (See below).

Since this is my personal web site there are a some other restrictions:
1. Do not use this to represent yourself as me.
2. Do not use this to imply that I support or approve of, well, anything.
3. You must include this readme, unaltered, in any distribution of this software.

** The MIT License (MIT) **

Copyright (c) 2014 Aaron C. Willows

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.


[NodeJS]:http://nodejs.org/
[Bower]:http://bower.io/
[GruntJS]:http://gruntjs.com/
[Yeoman]:http://yeoman.io/
[ngBoilerplate]:http://joshdmiller.github.io/ng-boilerplate/#/home