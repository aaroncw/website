/*
 * about.js
 *
 * Created: Sunday, May 04, 2014
 */

/**
 * @ngdoc overview
 * @name aaron.about
 * @description An empty module description. Please fill in a high level description of this module.
 * @requires ui.router
 */
angular.module('aaron.about', [
	'ui.router',
	'aaron.directives.skillBar'
])

/**
 * @ngdoc method
 * @name aaron.about.config
 * @methodOf aaron.about
 * @description This method configures the module by setting up the states contained in this module.
 */
	.config(function config($stateProvider) {
		$stateProvider.state('about', {
			url  : '/about',
			views: {
				"main": {
					controller : 'AboutController',
					templateUrl: 'about/about.tpl.html'
				}
			},
			data : { pageTitle: 'About' }
		});
	})

/**
 * @ngdoc method
 * @name aaron.about.AboutController
 * @methodOf aaron.about
 * @description The main controller of this module.
 */
	.controller('AboutController', function AboutController( $scope ) {



		$scope.devSkills = [
			{
				title: 'JavaScript',
				imageSrc: 'assets/images/icons/javascript.png',
				orientation: 'right',
				flavorText:'I have expert JavaScript knowledge, and have developed rich client side web applications using AngularJS.'
			},
			{
				title: 'Java',
				imageSrc: 'assets/images/icons/java.png',
				orientation: 'right',
				flavorText:'I have intermediate Java knowledge. I have developed web apps with Spring and Struts2 as well as RESTful interfaces.'
			},
			{
				title: 'Development',
				imageSrc: 'assets/images/icons/git.png',
				orientation: 'right',
				flavorText:'I have deep experience in the software development process, including continuous integration and source control. '
			}
		];

		//TODO: Credit for git logo
		$scope.webSkills = [
			{
				title: 'HTML5',
				imageSrc: 'assets/images/icons/html5.png',
				orientation: 'left',
				flavorText:'I am an expert in HTML5, including semantic markup and APIs like local storage.'
			},
			{
				title: 'CSS3',
				imageSrc: 'assets/images/icons/css3.svg',
				orientation: 'left',
				flavorText:'I have experience in writing efficient CSS and implementing responsive designs.'
			},
			{
				title: 'Design',
				imageSrc: 'assets/images/icons/pencil.png',
				orientation: 'left',
				flavorText:'I have basic design knowledge and experience implementing pixel perfect designs.'
			}
		];



		$scope.skills = _.zip($scope.devSkills, $scope.webSkills);

		console.log('aaron.about.AboutController Initialized');
	})

;

