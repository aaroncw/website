/*
 * projects.js
 *
 * Created: Sunday, May 04, 2014
 */

/**
 * @ngdoc overview
 * @name aaron.projects
 * @description An empty module description. Please fill in a high level description of this module.
 * @requires ui.router
 */
angular.module('aaron.projects', [
	'ui.router',
	'aaron.projects.nonogram'

])

/**
 * @ngdoc method
 * @name aaron.projects.config
 * @methodOf aaron.projects
 * @description This method configures the module by setting up the states contained in this module.
 */
	.config(function config($stateProvider) {
		$stateProvider.state('projects', {
			url  : '/projects',
			views: {
				"main": {
					controller : 'ProjectsController',
					templateUrl: 'projects/projects.tpl.html'
				}
			},
			data : { pageTitle: 'Projects' }
		});
	})

/**
 * @ngdoc method
 * @name aaron.projects.ProjectsController
 * @methodOf aaron.projects
 * @description The main controller of this module.
 */
	.controller('ProjectsController', function ProjectsController( $scope ) {
		console.log('aaron.projects.ProjectsController Initialized');
	})

;

