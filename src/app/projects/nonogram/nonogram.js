/*
 * nonogram.js
 *
 * Created: Saturday, May 31, 2014
 */

/**
 * @ngdoc overview
 * @name aaron.projects.nonogram
 * @description An empty module description. Please fill in a high level description of this module.
 * @requires ui.router
 */
angular.module('aaron.projects.nonogram', [
	'ui.router',
	'aaron.directives.nonogram'
])

/**
 * @ngdoc method
 * @name aaron.projects.nonogram.config
 * @methodOf aaron.projects.nonogram
 * @description This method configures the module by setting up the states contained in this module.
 */
	.config(function config($stateProvider) {
		$stateProvider.state('nonogram', {
			url  : '/nonogram',
			views: {
				"main": {
					controller : 'NonogramController',
					templateUrl: 'projects/nonogram/nonogram.tpl.html'
				}
			},
			data : { pageTitle: 'Nonogram' }
		});
	})

/**
 * @ngdoc method
 * @name aaron.projects.nonogram.NonogramController
 * @methodOf aaron.projects.nonogram
 * @description The main controller of this module.
 */
	.controller('NonogramController', function NonogramController( $scope ) {
		console.log('aaron.projects.nonogram.NonogramController Initialized');
	})

;

