/*
 * technical.js
 *
 * Created: Sunday, May 04, 2014
 */

/**
 * @ngdoc overview
 * @name aaron.technical
 * @description An empty module description. Please fill in a high level description of this module.
 * @requires ui.router
 */
angular.module('aaron.technical', [
	'ui.router'
])

/**
 * @ngdoc method
 * @name aaron.technical.config
 * @methodOf aaron.technical
 * @description This method configures the module by setting up the states contained in this module.
 */
	.config(function config($stateProvider) {
		$stateProvider.state('technical', {
			url  : '/technical',
			views: {
				"main": {
					controller : 'TechnicalController',
					templateUrl: 'technical/technical.tpl.html'
				}
			},
			data : { pageTitle: 'Technical' }
		});
	})

/**
 * @ngdoc method
 * @name aaron.technical.TechnicalController
 * @methodOf aaron.technical
 * @description The main controller of this module.
 */
	.controller('TechnicalController', function TechnicalController( $scope ) {
		console.log('aaron.technical.TechnicalController Initialized');
	})

;

