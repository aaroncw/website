/*
 * app.js
 *
 * Created: Sunday, May 04, 2014
 */


/**
 * @ngdoc overview
 * @name aaron
 * @description This is the root module for the application.
 * @requires templates-app'
 * @requires templates-common
 * @requires ui.router
 */
angular.module('aaron', [
	'templates-app',
	'templates-common',
	'ui.router',
	'Configuration',
	'aaron.about',
	'aaron.development',
	'aaron.resume',
	'aaron.design',
	'aaron.technical',
	'aaron.bootstrap',
	'aaron.projects',
	'aaron.attributions',
	'ui.bootstrap'
])
/**
 * @ngdoc method
 * @name aaron.config
 * @methodOf aaron
 * @description Use this function to configure the module.
 */
	.config(function myAppConfig($stateProvider, $urlRouterProvider, ApplicationProperties) {
		$urlRouterProvider.otherwise('/about');
	})

/**
 * @ngdoc method
 * @name aaron.run
 * @methodOf aaron
 * @description Code in this block is executed after all modules are configured.
 */
	.run(function run() {

	})

/**
 * @ngdoc method
 * @name aaron.MainController
 * @methodOf aaron.MainController
 * @description The main controller of this module and the application.
 */
	.controller('MainController', function MainController($scope, $location, $state) {
		console.log('Application: aaron started.');

		$scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
			if (angular.isDefined(toState.data.pageTitle)) {
				$scope.pageTitle = toState.data.pageTitle;
				$scope.browserTitle = 'Aaron - ' + $scope.pageTitle;
			}
		});


		$scope.alertOpen = true;
		$scope.closeAlert = function() {
			$scope.alertOpen = false;
		};

	})

;

//Initialize the application. This allows other code to be run before our Angular application runs.
angular.element(document).ready(function () {
	(function () {
		var method;
		var noop = function () {
		};
		var methods = [
			'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
			'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
			'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
			'timeStamp', 'trace', 'warn'
		];
		var length = methods.length;
		var console = (window.console = window.console || {});

		while (length--) {
			method = methods[length];

			// Only stub undefined methods.
			if (!console[method]) {
				console[method] = noop;
			}
		}
	}());
	angular.bootstrap(document, ['aaron']);
});