/*
 * attributions.js
 *
 * Created: Friday, May 16, 2014
 */

/**
 * @ngdoc overview
 * @name aaron.attributions
 * @description An empty module description. Please fill in a high level description of this module.
 * @requires ui.router
 */
angular.module('aaron.attributions', [
	'ui.router'
])

/**
 * @ngdoc method
 * @name aaron.attributions.config
 * @methodOf aaron.attributions
 * @description This method configures the module by setting up the states contained in this module.
 */
	.config(function config($stateProvider) {
		$stateProvider.state('attributions', {
			url  : '/attributions',
			views: {
				"main": {
					controller : 'AttributionsController',
					templateUrl: 'attributions/attributions.tpl.html'
				}
			},
			data : { pageTitle: 'Attributions' }
		});
	})

/**
 * @ngdoc method
 * @name aaron.attributions.AttributionsController
 * @methodOf aaron.attributions
 * @description The main controller of this module.
 */
	.controller('AttributionsController', function AttributionsController( $scope ) {
		console.log('aaron.attributions.AttributionsController Initialized');
	})

;

