describe( 'MainController', function() {
    describe( 'isCurrentUrl', function() {
        var MainController, $location, $scope;

        beforeEach(module('aaron'));

        beforeEach(inject(function($controller, _$location_, $rootScope) {
            $location = _$location_;
            $scope = $rootScope.$new();
            MainController = $controller('MainController', { $location: $location, $scope: $scope });
        }));

        it( 'should pass a dummy test', inject(function() {
            expect( MainController ).toBeTruthy();
        }));
    });
});
