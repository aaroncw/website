/*
 * resume.js
 *
 * Created: Sunday, May 04, 2014
 */

/**
 * @ngdoc overview
 * @name aaron.resume
 * @description An empty module description. Please fill in a high level description of this module.
 * @requires ui.router
 */
angular.module('aaron.resume', [
	'ui.router'
])

/**
 * @ngdoc method
 * @name aaron.resume.config
 * @methodOf aaron.resume
 * @description This method configures the module by setting up the states contained in this module.
 */
	.config(function config($stateProvider) {
		$stateProvider.state('resume', {
			url  : '/resume',
			views: {
				"main": {
					controller : 'ResumeController',
					templateUrl: 'resume/resume.tpl.html'
				}
			},
			data : { pageTitle: 'Resume' }
		});
	})

/**
 * @ngdoc method
 * @name aaron.resume.ResumeController
 * @methodOf aaron.resume
 * @description The main controller of this module.
 */
	.controller('ResumeController', function ResumeController( $scope ) {
		console.log('aaron.resume.ResumeController Initialized');
	})

;

