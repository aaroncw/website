/*
 * development.js
 *
 * Created: Sunday, May 04, 2014
 */

/**
 * @ngdoc overview
 * @name aaron.development
 * @description An empty module description. Please fill in a high level description of this module.
 * @requires ui.router
 */
angular.module('aaron.development', [
	'ui.router'
])

/**
 * @ngdoc method
 * @name aaron.development.config
 * @methodOf aaron.development
 * @description This method configures the module by setting up the states contained in this module.
 */
	.config(function config($stateProvider) {
		$stateProvider.state('development', {
			url  : '/development',
			views: {
				"main": {
					controller : 'DevelopmentController',
					templateUrl: 'development/development.tpl.html'
				}
			},
			data : { pageTitle: 'Development' }
		});
	})

/**
 * @ngdoc method
 * @name aaron.development.DevelopmentController
 * @methodOf aaron.development
 * @description The main controller of this module.
 */
	.controller('DevelopmentController', function DevelopmentController( $scope ) {
		console.log('aaron.development.DevelopmentController Initialized');
	})

;

