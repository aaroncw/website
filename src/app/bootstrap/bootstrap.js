/*
 * bootstrap.js
 *
 * Created: Sunday, May 04, 2014
 */

/**
 * @ngdoc overview
 * @name aaron.bootstrap
 * @description An empty module description. Please fill in a high level description of this module.
 * @requires ui.router
 */
angular.module('aaron.bootstrap', [
	'ui.router'
])

/**
 * @ngdoc method
 * @name aaron.bootstrap.config
 * @methodOf aaron.bootstrap
 * @description This method configures the module by setting up the states contained in this module.
 */
	.config(function config($stateProvider) {
		$stateProvider.state('bootstrap', {
			url  : '/bootstrap',
			views: {
				"main": {
					controller : 'BootstrapController',
					templateUrl: 'bootstrap/bootstrap.tpl.html'
				}
			},
			data : { pageTitle: 'Bootstrap' }
		});
	})

/**
 * @ngdoc method
 * @name aaron.bootstrap.BootstrapController
 * @methodOf aaron.bootstrap
 * @description The main controller of this module.
 */
	.controller('BootstrapController', function BootstrapController( $scope ) {
		console.log('aaron.bootstrap.BootstrapController Initialized');
	})

;

