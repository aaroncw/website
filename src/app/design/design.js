/*
 * design.js
 *
 * Created: Sunday, May 04, 2014
 */

/**
 * @ngdoc overview
 * @name aaron.design
 * @description An empty module description. Please fill in a high level description of this module.
 * @requires ui.router
 */
angular.module('aaron.design', [
	'ui.router'
])

/**
 * @ngdoc method
 * @name aaron.design.config
 * @methodOf aaron.design
 * @description This method configures the module by setting up the states contained in this module.
 */
	.config(function config($stateProvider) {
		$stateProvider.state('design', {
			url  : '/design',
			views: {
				"main": {
					controller : 'DesignController',
					templateUrl: 'design/design.tpl.html'
				}
			},
			data : { pageTitle: 'Design' }
		});
	})

/**
 * @ngdoc method
 * @name aaron.design.DesignController
 * @methodOf aaron.design
 * @description The main controller of this module.
 */
	.controller('DesignController', function DesignController( $scope ) {
		console.log('aaron.design.DesignController Initialized');
	})

;

