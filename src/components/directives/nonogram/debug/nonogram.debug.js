/*
 * nonogram.debug.js
 *
 * Created: Saturday, May 31, 2014
 */

/**
 * @ngdoc overview
 * @name aaron.directives.debug.nonogram
 * @description A debug module for the nonogram directive
 * @requires ui.router
 * @requires aaron.directives.nonogram
 */
angular.module('aaron.directives.debug.nonogram', [
	'ui.router',
	'aaron.directives.nonogram'
])

/**
 * @ngdoc method
 * @name aaron.directives.debug.nonogram.config
 * @methodOf aaron.directives.debug.nonogram
 * @description This method configures the debug module by setting up the states contained in this module.
 */
	.config(function config($stateProvider) {
		$stateProvider.state('nonogramDebug', {
			url  : '/debug/nonogram',
			views: {
				"main": {
					controller : 'NonogramDebugController',
					templateUrl: 'directives/nonogram/debug/nonogram.debug.tpl.html'
				}
			},
			data : { pageTitle: 'Nonogram Debug' }
		});
	})

/**
 * @ngdoc method
 * @name aaron.directives.debug.nonogram.NonogramDebugController
 * @methodOf aaron.directives.debug.nonogram
 * @description The main controller of this debug module.
 */
	.controller('NonogramDebugController', function NonogramDebugController( $scope ) {

	console.log('aaron.directives.debug.nonogram.NonogramDebugController Initialized');
	})

;
