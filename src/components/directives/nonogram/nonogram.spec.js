/*
 * nonogram.spec.js
 *
 * Created: Saturday, May 31, 2014
 */

/**
 * Tests sit right alongside the file they are testing, which is more intuitive
 * and portable than separating `src` and `test` directories. Additionally, the
 * build process will exclude all `.spec.js` files from the build
 * automatically.
 */
describe('nonogram section', function () {
	beforeEach(module('aaron.directives.nonogram'));

	it('should have a dummy test', inject(function () {
		expect(true).toBeTruthy();
	}));
});

