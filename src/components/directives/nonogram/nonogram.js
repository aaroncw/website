/*
 * nonogram.js
 *
 * Created: Saturday, May 31, 2014
 */

 /**
 * @ngdoc overview
 * @name aaron.directives.nonogram
 * @description An empty module description. Please fill in a high level description of this module.
 */ 
angular.module( 'aaron.directives.nonogram', [
	'ng-context-menu',
	'aaron.services.NonogramService'

])

 /**
 * @ngdoc directive 
 * @name aaron.directives.nonogram.nonogram
 * @restrict E
 * @element ANY 
 * @description An empty directive description. Please fill in a high level description of this directive.
 */ 
.directive('nonogram', function factory(NonogramService) { return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
        
    },
    templateUrl: 'directives/nonogram/nonogram.tpl.html',
    link: function(scope, element, attrs) {

		scope.puzzlesAvailable = [
			'duck.json',
			'dog.json'
//			'lambda.json'
		];

		scope.selectedPuzzle = '';
		scope.puzzle = {};
		scope.displayModes = {
			revealPicture: false,
			showWrongMoves: false
		};

		scope.puzzleState = {
			validState: false

		};

		scope.hints = { };


		scope.loadPuzzle = function() {

			scope.resetPuzzle();
			NonogramService.getPuzzle(scope.selectedPuzzle).then(
				function(data) {
					console.log('GET SUCCESS ' + scope.selectedPuzzle);
					console.dir(data);
					scope.puzzle = data;
					scope.createHints(data.grid);
					scope.getPuzzleState(scope.puzzle);
				},
				function(data) {
					console.log('GET FAIL ' + scope.selectedPuzzle);
					console.dir(data);
				}
			);
		};

		scope.resetPuzzle = function() {
			scope.puzzle = {};
			scope.displayModes = {
				revealPicture: false,
				showWrongMoves: false
			};

			scope.puzzleState = {
				validState: false

			};

			scope.hints = { };
		};

		scope.showBadCell = function(cellData) {
			if(scope.displayModes.showWrongMoves) {
				var isWrong = (cellData.actualState == 'marked') && (cellData.correctState != 'marked');
				isWrong = isWrong || (cellData.actualState == 'no') && (cellData.correctState == 'marked');

				return isWrong;
			} else {
				return false;
			}
		};

		scope.clearGrid = function() {
			angular.forEach(scope.puzzle.grid, function(value, key) {
				angular.forEach(value, function(value, key) {
					value.actualState = 'unmarked';
				});
			});

			scope.getPuzzleState(scope.puzzle);
		};




		scope.onRightClick = function(event) {
			var eventElement = angular.element(event.target);
			var rowIndex = eventElement.attr('data-row');
			var columnIndex = eventElement.attr('data-column');

			if(!scope.puzzleState.complete) {
				if (scope.puzzle.grid[rowIndex][columnIndex].actualState == 'no') {
					scope.puzzle.grid[rowIndex][columnIndex].actualState = 'unmarked';
					scope.puzzleState.unmarkedCellsLeft++;
				} else {
					scope.puzzle.grid[rowIndex][columnIndex].actualState = 'no';
					scope.puzzleState.unmarkedCellsLeft--;
				}

				scope.puzzleIsComplete(scope.puzzleState);
			}

			console.log('Right Click [rowIndex=' + rowIndex + '][columnIndex=' + columnIndex + '][complete=' + scope.puzzleState.complete + ']');
			event.preventDefault();
			return false;
		};

		scope.onClick = function(event) {
			var eventElement = angular.element(event.target);
			var rowIndex = eventElement.attr('data-row');
			var columnIndex = eventElement.attr('data-column');

			if(!scope.puzzleState.complete) {
				if (scope.puzzle.grid[rowIndex][columnIndex].actualState == 'marked') {
					scope.puzzle.grid[rowIndex][columnIndex].actualState = 'unmarked';
					scope.puzzleState.markedCellsLeft++;
				} else {
					scope.puzzle.grid[rowIndex][columnIndex].actualState = 'marked';
					scope.puzzleState.markedCellsLeft--;
				}

				scope.puzzleIsComplete(scope.puzzleState);
			}

			console.log('Left Click [rowIndex=' + rowIndex + '][columnIndex=' + columnIndex + '][complete=' + scope.puzzleState.complete + ']');
			event.preventDefault();
			return false;
		};



		var createRowHints = function(grid, result) {
			for(var rowIndex = 0; rowIndex < grid.length; rowIndex++) {
				var runLength = 0;

				result.rows.hints.push([]);
				for(var columnIndex = 0; columnIndex < grid[rowIndex].length; columnIndex++) {
					var cell = grid[rowIndex][columnIndex];

					if(cell.correctState == 'marked'){
						runLength++;
					}

					var runOver = cell.correctState == 'unmarked';
					runOver = runOver || columnIndex == (grid[rowIndex].length - 1);
					runOver = runOver && (runLength > 0);

					if(runOver) {
						result.rows.hints[rowIndex].push(runLength);
						runLength = 0;

						if(result.rows.hints[rowIndex].length > result.rows.maxLength) {
							result.rows.maxLength = result.rows.hints[rowIndex].length;
						}
					}
				}
			}

			for(var x = 0; x < result.rows.hints.length; x++) {
				while(result.rows.hints[x].length < result.rows.maxLength) {
					result.rows.hints[x].unshift(' ');
				}
			}
		};

		var createColumnHints = function(grid, result) {
			for(var columnIndex = 0; columnIndex < grid.length; columnIndex++) {
				var runLength = 0;
				var runCounter = 0;

				for(var rowIndex = 0; rowIndex < grid[columnIndex].length; rowIndex++) {
					var cell = grid[rowIndex][columnIndex];

					if(cell.correctState == 'marked') {
						runLength++;
					}

					var runOver = cell.correctState == 'unmarked';
					runOver = runOver || (rowIndex == (grid[columnIndex].length - 1));
					runOver = runOver && (runLength > 0);

					if(runOver) {
						if(result.columns.hints.length <= runCounter) {
							var newRow = [];
							newRow.length = grid[columnIndex].length;
							result.columns.hints.push(newRow);
						}

						result.columns.hints[runCounter][columnIndex] = runLength;
						runLength = 0;
						runCounter++;
					}
				}
			}

			result.columns.maxLength = result.columns.hints.length;
			for(var hintsIndex = 0; hintsIndex < result.columns.hints.length; hintsIndex++) {
				for(var index = 0; index < result.columns.hints[hintsIndex].length; index++) {
					if(!result.columns.hints[hintsIndex][index]) {
						result.columns.hints[hintsIndex][index] = ' ';
					}
				}
			}

			for(var x = 0; x < (result.columns.hints.length - 1); x++) {
				console.log('[result.columns.hints[' + x + '].length=' + result.columns.hints[x].length + ']');

				for(var y = 0; y < result.columns.hints[x].length; y++) {
					if(result.columns.hints[x + 1][y] == ' ') {
						result.columns.hints[x + 1][y] = result.columns.hints[x][y];
						result.columns.hints[x][y] = ' ';
					}
				}

				while(result.columns.hints[x].length < result.columns.maxLength) {
					result.columns.hints[x].unshift(' ');
				}
			}
		};

		scope.createHints = function(grid) {
			var result = {
				columns: {
					maxLength: 0,
					hints: []
				},
				rows: {
					maxLength: 0,
					hints: []
				}
			};

			createRowHints(grid, result);
			createColumnHints(grid, result);

			scope.hints = result;

			return result;
		};

		scope.getPuzzleState = function(puzzle) {
			var result = {
				markedCellsLeft: 0,
				totalMarkedCells: 0,

				unmarkedCellsLeft: 0,
				totalUnmarkedCells: 0,

				validState: true,
				complete: false

			};

			for(var x = 0; x < (puzzle.grid.length); x++) {
				for(var y = 0; y < puzzle.grid[x].length; y++) {
					if(puzzle.grid[x][y].correctState == 'unmarked') {
						result.unmarkedCellsLeft++;
					} else if(puzzle.grid[x][y].correctState == 'marked') {
						result.markedCellsLeft++;
					}
				}
			}

			result.totalMarkedCells = result.markedCellsLeft;
			result.totalUnmarkedCells = result.unmarkedCellsLeft;
			scope.puzzleState = result;
		};

		scope.puzzleIsComplete = function(puzzleState) {
			if(!puzzleState.validState) {
				return false;
			}

			if(puzzleState.totalUnmarkedCells != puzzleState.unmarkedCellsLeft) {
				return false;
			}

			puzzleState.complete = puzzleState.markedCellsLeft === 0;
			return puzzleState.markedCellsLeft === 0;
		};




    }
};});