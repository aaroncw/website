/*
 * skillBar.js
 *
 * Created: Wednesday, May 07, 2014
 */

 /**
 * @ngdoc overview
 * @name aaron.directives.skillBar
 * @description An empty module description. Please fill in a high level description of this module.
 */ 
angular.module( 'aaron.directives.skillBar', [


])

 /**
 * @ngdoc directive 
 * @name aaron.directives.skillBar.skillBar
 * @restrict E
 * @element ANY 
 * @description An empty directive description. Please fill in a high level description of this directive.
 */ 
.directive('skillBar', function factory() { return {
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {
		orientation: '=orientation',
		titleText:'=titleText',
		imageSrc:'=imageSrc',
		flavorText:'=flavorText'
    },
    templateUrl: 'directives/skillBar/skillBar.tpl.html',
    link: function(scope, element, attrs) {
       
    }
};});