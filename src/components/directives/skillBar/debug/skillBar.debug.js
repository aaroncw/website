/*
 * skillBar.debug.js
 *
 * Created: Wednesday, May 07, 2014
 */

/**
 * @ngdoc overview
 * @name aaron.directives.debug.skillBar
 * @description A debug module for the skillBar directive
 * @requires ui.router
 * @requires aaron.directives.skillBar
 */
angular.module('aaron.directives.debug.skillBar', [
	'ui.router',
	'aaron.directives.skillBar'
])

/**
 * @ngdoc method
 * @name aaron.directives.debug.skillBar.config
 * @methodOf aaron.directives.debug.skillBar
 * @description This method configures the debug module by setting up the states contained in this module.
 */
	.config(function config($stateProvider) {
		$stateProvider.state('skillBarDebug', {
			url  : '/debug/skillBar',
			views: {
				"main": {
					controller : 'SkillBarDebugController',
					templateUrl: 'directives/skillBar/debug/skillBar.debug.tpl.html'
				}
			},
			data : { pageTitle: 'SkillBar Debug' }
		});
	})

/**
 * @ngdoc method
 * @name aaron.directives.debug.skillBar.SkillBarDebugController
 * @methodOf aaron.directives.debug.skillBar
 * @description The main controller of this debug module.
 */
	.controller('SkillBarDebugController', function SkillBarDebugController( $scope ) {

	console.log('aaron.directives.debug.skillBar.SkillBarDebugController Initialized');
	})

;
