/*
 * NonogramService.spec.js
 *
 * Created: Thursday, June 05, 2014
 */

/**
 * Tests sit right alongside the file they are testing, which is more intuitive
 * and portable than separating `src` and `test` directories. Additionally, the
 * build process will exclude all `.spec.js` files from the build
 * automatically.
 */
describe('NonogramService section', function () {
	beforeEach(module('aaron.services.NonogramService'));

	it('should have a dummy test', inject(function () {
		expect(true).toBeTruthy();
	}));
});

