/*
 * NonogramService.js
 *
 * Created: Thursday, June 05, 2014
 */

/**
 * @ngdoc overview
 * @name aaron.services.NonogramService
 * @description An empty module description. Please fill in a high level description of this module.
 */
angular.module('aaron.services.NonogramService', [


])

/**
 * @ngdoc service
 * @name aaron.services.NonogramService
 * @description An empty service description. Please fill in a high level description of this service.
 */
	.service('NonogramService', [ '$q', '$http', function ($q, $http) {
		return {

			getPuzzle: function(fileName) {
				var puzzelPath = 'assets/nonograms/' + fileName;
				var deferred = $q.defer();
				var self = this;

				$http({method: 'GET', url: puzzelPath}).
					success(function(data, status, headers, config) {
						deferred.resolve(data);
					}).
					error(function(data, status, headers, config) {
						deferred.reject(data);
					});

				return deferred.promise;
			}

		};
	}]);
