window.applicationProperties = (function () {
	return {
		configurationVersion: '0.0.1'

	};
})();

angular.module('Configuration', []).constant('ApplicationProperties', window.applicationProperties);